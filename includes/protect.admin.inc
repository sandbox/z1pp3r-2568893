<?php

function protect_settings_form($form, &$form_state) {

  $options = array(
    'on' => t('On'),
    'off' => t('Off')
  );

  $options_behaviour = array(
    'bypass' => t('Normal Drupal Behaviour'),
    'delete' => t('Soft Delete'),
    'protect' => t('Disallow delete')
  );

  $form['protect_default_behaviour'] = array(
    '#type' => 'select',
    '#title' => t('Default protect behaviour for site.'),
    '#description' => t('Set default protect behaviour to be used on all items unless overridden below'),
    '#options' => $options_behaviour,
    '#default_value' => variable_get('protect_default_behaviour'),
    '#required' => TRUE,
  );

  $form['protect_default_edit_permissions'] = array(
    '#type' => 'select',
    '#title' => t('Default edit permission behaviour for site.'),
    '#description' => t('Set default protect behaviour for locking editing. To be applied to all items unless overridden below'),
    '#options' => $options,
    '#default_value' => variable_get('protect_default_edit_permissions'),
    '#required' => TRUE,
  );


  return system_settings_form($form);
}
