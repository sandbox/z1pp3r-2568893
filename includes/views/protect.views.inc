<?php


/**
 * Implements hook_views_data().
 */
function protect_views_data()
{

  $data = array();

  $data['protect_delete']['table']['group'] = t('Protect');

  $data['protect_delete']['table']['base'] = array(
    'title' => t('Protect'),
    'help' => t('Deleted protected nodes.'),
  );


  $data['protect_delete']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  // The Name field
  $data['protect_delete']['title'] = array(
    'title' => t('Title'),
    'help' => t('The node title of teh deleted node '),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );


  $data['protect_delete']['nid'] = array(
    'title' => t('Node ID'),
    'help' => t('The record node ID.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Node'),
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
  );

  return $data;

}


//end